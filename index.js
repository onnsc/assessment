var path = require("path");
var express= require("express");

//load an instance of app express
var app = express();

//creating the whole form array of objects
var allUser=[];

//actually submitting the form aka createOrder
var submitForm=function(user){
    return({
        email: user.email,
        password: user.password,
        name: user.name,
        gender: user.gender,
        dob: user.dob,
        address: user.address,
        country: user.country,
        contactno: user.contactno,
    });
    
} //close bracket for submitForm

app.get("/submit-button", function(req,resp){
    allUser.push(submitForm(req.query));
    console.log("All registered users: %s", JSON.stringify(allUser));
    resp.status(201).end();
});

app.get('/view-user', function(req,resp, $http){
    resp.status(200)
    resp.type("application/json");
    resp.json(allUser);
    console.log("All registered users: %s", JSON.stringify(allUser));
});

/*var date = new Date();

var day = date.getDate();
var month = date.getMonth() + 1;
var year = date.getFullYear() + 18;

if (month < 10) month = "0" + month;
if (day < 10) day = "0" + day;

var yearslater = year + "-" + month + "-" + day;       
document.getElementById("theDate").value = yearslater;*/


//link to html file
app.use(express.static(path.join(__dirname,"public")));
app.use("/libs", express.static(path.join(__dirname, "bower_components")));


//set the port
app.set("port", parseInt(process.argv[2] || 3000));

//listen to port
app.listen(app.get("port"),function(){
    console.log("Application is on port %d at %s", app.get("port"), new Date())

});

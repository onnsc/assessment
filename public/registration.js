(function(){
    console.log("checking that this angular.js file works")
    
    //creating the app
    var UserApp = angular.module("UserApp", []);

    //initialize form
    var initForm=function(placeholder){
        placeholder.email="";
        placeholder.password="";
        placeholder.name="";
        placeholder.gender="";
        placeholder.dob="";
        placeholder.address="";
        placeholder.country="";
        placeholder.contactno="";
    } //close bracket for initForm

    

    //creating the form OBJECT, allocating keys to values
    //aka creating the query stringg
    var createFormObject = function(placeholder){
        return({
            email: placeholder.email,
            password: placeholder.password,
            name: placeholder.name,
            gender: placeholder.gender,
            dob: placeholder.dob,
            address: placeholder.address,
            country: placeholder.country,
            contactno: placeholder.contactno,
        });
    } //close bracket for createFormObject

    //creating the controller
    var UserCtrl=function($http){
        var userCtrl=this;

        initForm(userCtrl);

        userCtrl.submitButton=function(){
            //the $http
            $http.get("/submit-button", {
                params: createFormObject(userCtrl)
            });
            return(createFormObject)
            
        };//curly bracket for userCtrl.submitButton

    //view all users
        userCtrl.viewUser=[];

        userCtrl.viewUser=function(){
            $http.get("/view-user")
                .then(function(res){
                    userCtrl.viewUser=res.data;
                    userCtrl.ready=true;
                })
                .catch(function(dummy){
                    console.log(Mehhhhhhh)
                })                
        };


//the curly bracket below is for controller. Dont touch it
    }



    UserApp.controller("UserCtrl", [ "$http", UserCtrl ]);


})();